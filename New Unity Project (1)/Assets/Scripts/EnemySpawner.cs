using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class attacked to a simple object, such as a rectangle, to be used as a spawn zone for enemies
/// </summary>
public class EnemySpawner : MonoBehaviour
{
    // Enemy to spawn
    [SerializeField]
    private GameObject enemyToSpawn;

    // Spawn rate
    [SerializeField]
    private float spawnRate = 6f;

    // Object renderer
    private Renderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();

        // Turns visibility off
        renderer.enabled = false;

        // Will repeatedly call the Spawn Function every set increment in time
        InvokeRepeating("SpawnEnemy", 0.5f, spawnRate);
    }

    // Update is called once per frame
    void SpawnEnemy()
    {
        float x1 = transform.position.x - renderer.bounds.size.x / 2;
        float x2 = transform.position.x + renderer.bounds.size.x / 2;

        // Randomly pick a point within the spawn object
        Vector2 spawnPoint = new Vector2(Random.Range(x1, x2), transform.position.y);

        // Spawn the object at the 'spawnPoint' position
        Instantiate(enemyToSpawn, spawnPoint, Quaternion.identity);
    }
}
