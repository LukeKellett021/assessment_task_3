using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    // Player's current score
    private float score;

    // Player's current combo count
    private int combo;

    [SerializeField]
    private Slider playerHpSlider;

    [SerializeField]
    public Difficulty difficulty {get; private set;}

    [SerializeField]
    private TextMeshProUGUI scoreTextUI;

    [SerializeField]
    private TextMeshProUGUI comboTextUI;

    // Start is called before the first frame update
    void Start()
    {
        //get Difficulty from start menu
        score = 0;
        combo = 0;
        //set up player HP bar
        playerHpSlider.value = 1;
        scoreTextUI.text = score.ToString();
        comboTextUI.text = combo.ToString();
    }

    /// <summary>
    /// Changes the current difficulty of the game.  Would be set at the start of the game.
    /// </summary>
    public void SetDifficulty(Difficulty newDifficulty)
    {
        difficulty = newDifficulty;
    }

    /// <summary>
    ///  Increases the Player's score by the specified float. Also updates the UI element with the new total score.
    /// </summary>
    void IncreaseScore(float ScoreToAdd)
    {
        score = ScoreToAdd * getMultiplier(combo);
        //Score additonal points with higher difficulties.
        switch (difficulty)
        {
            case Difficulty.Medium:
                score = score * 1.25f;
                break;
            case Difficulty.Hard:
                score = score * 1.5f;
                break;
            default:
                break;
        }
        if (score < 0)
        {
            score = 0;
        }
        score = Mathf.Round(score);
        scoreTextUI.text = score.ToString();
    }

    // Resets the score variable and UI to 0;
    void ResetScore()
    {
        score = 0;
        scoreTextUI.text = score.ToString();
    }

    /// <summary>
    ///  Increases the Player's combo count by one. Also updates the UI element with the new combo amount.
    /// </summary>
    void IncreaseCombo()
    {
        combo++;
        scoreTextUI.text = score.ToString();
    }

    // Resets the combo variable and UI to 0;
    void RestartCombo()
    {
        combo = 0;
        comboTextUI.text = combo.ToString();
    }

    /// <summary>
    /// returns the score multiplier derived from the player's current combo count.
    /// </summary>
    /// <param name="comboValue"></param>
    /// <returns></returns>
    int getMultiplier(int comboValue)
    {
        var multiplier = 1 + (comboValue / 10);
        return multiplier;
    }

    public void setHpBarValue(float currentHP, float maxHP)
    {
        playerHpSlider.value = currentHP / maxHP;
    }

}

/// <summary>
/// An enum that defines difficulty selection. Can pick between three difficulty options.
/// </summary>
public enum Difficulty
{
    Easy,
    Medium,
    Hard
}