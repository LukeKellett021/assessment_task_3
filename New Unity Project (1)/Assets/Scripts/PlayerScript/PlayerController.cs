using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Health healthComponent;

    public GameManager gameManager;

    public bool isAttacking = false;
    public Animator myAnim;

    public MovementScript movement;
    public static PlayerController instance;

    public bool isMoving = false;
    
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
        healthComponent = GetComponent<Health>();
        gameManager = GetComponent<GameManager>();
        movement = GetComponent<MovementScript>();
        myAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalInput = Input.GetAxis("Horizontal");
        if (movement != null) {
            /** Old functions, we combined both checks into the check below
            if (horizontalInput != 0.0f)
            {
                
                movement.MoveRight(horizontalInput);
            }

            if (verticalInput != 0.0f)
            {
                
                movement.MoveUp(verticalInput);
            }
            **/
            if (horizontalInput != 0.0f || verticalInput != 0.0f)
            {
                movement.Move(horizontalInput, verticalInput);
                isMoving = true;
            }
        }
        if (Input.GetButtonDown("Attack") && !isAttacking)
        {
            isAttacking = true;
        }
        
    }

    void TakeDamage(int damage)
    {
        healthComponent.TakeDamage(damage);
        gameManager.setHpBarValue(healthComponent.HP, healthComponent.MaxHP);
    }
}
