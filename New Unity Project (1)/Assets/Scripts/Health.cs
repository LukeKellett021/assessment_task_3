using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour, IHealth
{
    [SerializeField]
    public int MaxHP;
    //HP Can be set in unity editor, but will be overwritten at runtime by MaxHP;
    public int HP; 
    // Start is called before the first frame update
    void Start()
    {
        HP = MaxHP;
    
    }

    private void Update()
    {
        if (HP <= 0)
        {
            Die();
        }
    }

    /// <summary>
    /// Called when the player reaches 0HP. Plays a death animation?
    /// </summary>
    public void Die()
    {
        //Die Here
        //Get component PlayerContoller.DeathAnimation()
        //    "    "     .Die()
    }

    /// <summary>
    /// Decreases HP by the given integer. HP won't be set lower then 0.
    /// </summary>
    public void TakeDamage(int damage)
    {
        HP = HP - damage;
        if (HP < 0)
        {
            HP = 0;
        }
    }
}
