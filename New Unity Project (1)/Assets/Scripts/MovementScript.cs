using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    [SerializeField]
    private float horizontalVelocity = 20f;
    [SerializeField]
    private float verticalVelocity = 5f;

    private Rigidbody2D rb;

    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {

    }
    /** CREDIT to Declan for orignally writing these, these worked good but we ended up changing it.
    public void MoveRight(float horizontalInput)
    {
        //if (horizontalInput != 0.0f)
        //{
            //Vector2 forceToAdd = Vector2.right * 10* horizontalInput * horizontalVelocity * Time.deltaTime;
            Vector2 newForce = new Vector2(rb.position.x + 1f * horizontalInput, rb.position.y);
            rb.MovePosition(newForce);
            //Debug.Log("Trying to move horizontal");
        //}
    }
    public void MoveUp(float verticalInput)
    {
        //if (verticalInput != 0.0f)
        //{
        //Vector2 forceToAdd = Vector2.up  * 10 * verticalInput * verticalVelocity * Time.deltaTime;
        Vector2 newForce = new Vector2(rb.position.x, rb.position.y + 1f * verticalInput);
        rb.MovePosition(newForce);
        //Debug.Log("Trying to move vertical");
        //}
    }
    **/
    public void Move(float horizontalInput, float verticalInput)
    {
        Vector2 newForce = new Vector2(rb.position.x + 1f * horizontalInput, rb.position.y + 1f * verticalInput);
        rb.MovePosition(newForce);
    }
}
