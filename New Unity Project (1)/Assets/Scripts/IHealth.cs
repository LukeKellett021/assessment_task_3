using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for objects that are able to take damage.
/// Garantees object will have current and maximum health,
/// and methods for taking damage and dying.
/// </summary>
public interface IHealth {

    void TakeDamage(int damage);

    void Die();
}
